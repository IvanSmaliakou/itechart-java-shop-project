# A simple shop implementation on Java
##Requirements
- Simple console menu
- All pages but "login" available for logged users.
- Implementation of logging/registration
- Users should be able to execute read ops
- Admins can do all CRUD ops
- Additional menu for admins
- Validation for new users/products
- Custom exceptions must be handled
- Implementation of logging of all events